from tensorflow import keras
import keras.layers
import tensorflow as tf
import numpy as np
import tensorflow_datasets as tfds
from keras.utils import to_categorical
from keras.utils import Sequence
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras import layers, models
from keras.callbacks import EarlyStopping
from PIL import Image
import os
import math

model = keras.models.load_model('model.h5')

INPUT_DIRECTORY = "images/out"

all_images = [f.path for f in os.scandir(INPUT_DIRECTORY) if f.is_dir()]
np.random.shuffle(all_images)

test_frac = 0.2
test_images = all_images[:int(test_frac * len(all_images))]
train_images = all_images[int(test_frac * len(all_images)):]
validation_frac = 0.1
validation_images = train_images[:int(validation_frac * len(train_images))]
train_images = train_images[int(validation_frac * len(train_images)):]


# TODO: dit geeft geen random lateral piece examples!!!!
def generate_dataset(images, predict=False):
    for subdir in images:
        fragment_paths = os.listdir(subdir)
        amount_fragments_side = math.sqrt(len(fragment_paths))
        center_fragment_index = str(int(amount_fragments_side / 2))
        center_fragment_name = subdir.split('/')[
                                   -1] + "_" + center_fragment_index + "_" + center_fragment_index + ".jpg"
        center_fragment = np.array(Image.open(os.path.join(subdir, center_fragment_name)))
        for file in os.listdir(subdir):
            if not file.endswith(center_fragment_name):
                ss = file.split('.')[0].split('_')
                fragment_index = int(ss[-2]) * amount_fragments_side + int(ss[-1])
                fragment_index = fragment_index if fragment_index <= int(
                    math.pow(amount_fragments_side, 2) / 2) else fragment_index - 1
                lateral_fragment = np.array(Image.open(os.path.join(subdir, file)))
                x = [center_fragment / 255.0, lateral_fragment / 255.0]
                y = to_categorical(fragment_index, num_classes=int(math.pow(amount_fragments_side, 2) - 1))
                yield x if predict else [x, y]


# # ===============================

# Network
single_image_shape = (96, 96, 3)

test_frac = 0.2
test_images = all_images[:int(test_frac * len(all_images))]
test_gen = generate_dataset(test_images)

# x1_test = []
# x2_test = []
# y_test = []
# print("Creating test set")
# i = 0
# for ex in test_gen:
#     x1_test.append(ex[0][0])
#     x2_test.append(ex[0][1])
#     y_test.append(ex[1])
#     if i % 100 == 0:
#         print(i)
#     if i > 2000:  # Mem limit
#         break
#     i += 1
# model.evaluate(
#     [np.array(x1_test), np.array(x2_test)],
#     np.array(y_test)
    
# )

i = 0
for ex in test_gen:
    print(
        model.predict_on_batch([np.array([ex[0][0]],), np.array([ex[0][1],])])
    )
    print(ex[1])
    if i > 5:
        break
    i += 1

