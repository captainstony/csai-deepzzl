# https://towardsdatascience.com/transfer-learning-with-vgg16-and-keras-50ea161580b4
import keras.layers
import tensorflow as tf
import numpy as np
import tensorflow_datasets as tfds
from keras.utils import to_categorical
from keras.utils import Sequence
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras import layers, models
from keras.callbacks import EarlyStopping
from PIL import Image
import os
import math

INPUT_DIRECTORY = "images/out"

all_images = [f.path for f in os.scandir(INPUT_DIRECTORY) if f.is_dir()]
np.random.shuffle(all_images)

test_frac = 0.2
test_images = all_images[:int(test_frac * len(all_images))]
train_images = all_images[int(test_frac * len(all_images)):]
validation_frac = 0.1
validation_images = train_images[:int(validation_frac * len(train_images))]
train_images = train_images[int(validation_frac * len(train_images)):]


# TODO: dit geeft geen random lateral piece examples!!!!
def generate_dataset(images, predict=False):
    for subdir in images:
        fragment_paths = os.listdir(subdir)
        amount_fragments_side = math.sqrt(len(fragment_paths))
        center_fragment_index = str(int(amount_fragments_side / 2))
        center_fragment_name = subdir.split('/')[
                                   -1] + "_" + center_fragment_index + "_" + center_fragment_index + ".jpg"
        center_fragment = np.array(Image.open(os.path.join(subdir, center_fragment_name)))
        for file in os.listdir(subdir):
            if not file.endswith(center_fragment_name):
                ss = file.split('.')[0].split('_')
                fragment_index = int(ss[-2]) * amount_fragments_side + int(ss[-1])
                fragment_index = fragment_index if fragment_index <= int(
                    math.pow(amount_fragments_side, 2) / 2) else fragment_index - 1
                lateral_fragment = np.array(Image.open(os.path.join(subdir, file)))
                x = [center_fragment / 255.0, lateral_fragment / 255.0]
                y = to_categorical(fragment_index, num_classes=int(math.pow(amount_fragments_side, 2) - 1))
                yield x if predict else [x, y]


# # ===============================

# Network
single_image_shape = (96, 96, 3)

fen1 = VGG16(weights="imagenet", include_top=False, input_shape=single_image_shape)
# TODO: Probeer eens met True
fen1.trainable = False
fen1._name = "vgg16_1"
fen2 = VGG16(weights="imagenet", include_top=False, input_shape=single_image_shape)
fen2.trainable = False
fen2._name = "vgg16_2"

inputA = layers.Input(shape=single_image_shape)
inputB = layers.Input(shape=single_image_shape)

x = fen1(inputA)
x = keras.Model(inputs=inputA, outputs=x)
y = fen2(inputB)
y = keras.Model(inputs=inputB, outputs=y)

x_flat = layers.Flatten()(x.output)
y_flat = layers.Flatten()(y.output)

# TODO: Als de acc nog altijd slecht is, verander 512 naar 4k of 8k
shared = layers.Dense(512, activation="relu")
x_s = shared(x_flat)
y_s = shared(y_flat)
# TODO: Als het vorige niet werkt, dan:
# In practice, the outer product works better but costs a lot more. It works by
# reshaping one entry to (512,1) and the other to (1,512) and then multiplying.
# Because of broadcast, you get an output of size (512,512) that you can then
# flatten leading to a huge vector of size 256k. This means the first dense
# post-fusion is of size 256k->512 which is a lot of parameters... It may be also
# viable by reducing the input dimension further before the Kronecker product to,
# e.g., 64. That way the fusion leads to 64x64=4k dimension and the next dense
# post fusion is 4k->512 which is reasonable.
combined = layers.Multiply()([x_s, y_s])

z = layers.Dense(512, activation="relu")(combined)
z = layers.BatchNormalization()(z)
z = layers.Dense(512, activation="relu")(z)
z = layers.BatchNormalization()(z)
output = layers.Dense(8, activation="softmax")(z)

model = keras.Model(inputs=[x.input, y.input], outputs=output)
keras.utils.plot_model(model, "info.png", show_shapes=True)
# print(model.summary())
model.compile(
    optimizer="adam",
    loss="categorical_crossentropy",
    metrics=["accuracy"],
)

es = EarlyStopping(
    monitor="val_accuracy", mode="max", patience=5, restore_best_weights=True
)

# =====================
train_gen = generate_dataset(train_images, predict=False)
validate_gen = generate_dataset(validation_images, predict=False)

x1_validate = []
x2_validate = []
y_validate = []
print("Creating validation set")
i = 0
for ex in validate_gen:
    x1_validate.append(ex[0][0])
    x2_validate.append(ex[0][1])
    y_validate.append(ex[1])
    if i % 100 == 0:
        print(i)
    if i > 2000:  # Mem limit
        break
    i += 1
# https://github.com/keras-team/keras/issues/107
x1_batch = []
x2_batch = []
y_batch = []
epoch = 0
batch_size = 64
for ex in train_gen:
    x1_batch.append(ex[0][0])
    x2_batch.append(ex[0][1])
    y_batch.append(ex[1])
    if len(x1_batch) >= batch_size:  # Batch size
        print("Epoch: " + str(epoch))
        epoch += 1
        history = model.fit(
            [np.array(x1_batch), np.array(x2_batch)],
            np.array(y_batch),
            batch_size,
            validation_data=([np.array(x1_validate), np.array(x2_validate)], np.array(y_validate)),
            epochs=1,
            verbose=0,
        )
        print(history.history)
        x1_batch, x2_batch, y_batch = [], [], []
        if epoch % 50:
            model.save('model.h5')
        if epoch > 300:  # Max epochs
            break

model.save('model.h5')
x_batch, y_batch, x_validate, y_validate = [], [], [], []  # Clear mem
